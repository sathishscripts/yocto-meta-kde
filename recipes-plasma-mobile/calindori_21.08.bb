# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/calindori-21.08.tar.xz"
SRC_URI[sha256sum] = "2b8cea4f2da4ff27be9251a1324bc6aac8cffeed3e73fc3cb202c0a991324123"


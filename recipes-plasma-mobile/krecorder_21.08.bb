# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/krecorder-21.08.tar.xz"
SRC_URI[sha256sum] = "d1088c33f3a6e4306eaf1a14736eee71edf239c4b11c348d53fa0c9810ea018d"


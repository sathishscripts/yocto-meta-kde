# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/kweather-21.08.tar.xz"
SRC_URI[sha256sum] = "c5c572b38d6e74e15998273fcff9438711b0f6f550fe23908156f3a934973f2c"


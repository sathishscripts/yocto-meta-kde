# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/plasma-settings-21.08.tar.xz"
SRC_URI[sha256sum] = "52fcab15a648b0d36568825a74b92214d1a1e48d5cdc72d0042bd19ffd0bbb00"


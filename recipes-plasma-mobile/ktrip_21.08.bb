# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/ktrip-21.08.tar.xz"
SRC_URI[sha256sum] = "1d199f11588ec53c96f12aa06c3b2d6832cf074f32398907b6bf9e86ef88fd46"


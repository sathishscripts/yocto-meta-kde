# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/koko-21.08.tar.xz"
SRC_URI[sha256sum] = "d79c2d298662dca14226c9c9d3c86901f4c85d1f0ac3396da6cdd470106514eb"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/kongress-21.08.tar.xz"
SRC_URI[sha256sum] = "6a8effaa68ad0e5ba070bcf969d09956347e02dbac43245c10fd59b217d12d25"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/kclock-21.08.tar.xz"
SRC_URI[sha256sum] = "ed901efdfbf7c8d3164443815cbecc156e4e7ad85563f79a59839ec6697700ff"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/alligator-21.08.tar.xz"
SRC_URI[sha256sum] = "1858d5a3863c7c92eaa4302bd0f808f2a03fd359cfdf46f5e895971d1a771cb6"


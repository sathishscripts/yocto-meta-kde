# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma-mobile/21.08/kasts-21.08.tar.xz"
SRC_URI[sha256sum] = "762bfc029455b87d564e4e9e6f4ca0e7e9150e1716c4b63e8cd690cd3b8b6683"


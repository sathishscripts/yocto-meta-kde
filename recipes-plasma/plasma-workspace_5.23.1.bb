# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-workspace-5.23.1.tar.xz"
SRC_URI[sha256sum] = "fbf979f857b3cdcc8fe6dc45c3a1d9dfe297e1120aa551a02faa339abb81efad"


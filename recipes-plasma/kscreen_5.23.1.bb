# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kscreen-5.23.1.tar.xz"
SRC_URI[sha256sum] = "af143f2d3b25478437ff894b0afe094a69df8dab055bba3c6311d0735f431fc9"


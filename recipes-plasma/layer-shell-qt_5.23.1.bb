# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/layer-shell-qt-5.23.1.tar.xz"
SRC_URI[sha256sum] = "c79c2306ee9f6a672612b37f28074609748f5570b7785d4096e5573f12fb9e0a"


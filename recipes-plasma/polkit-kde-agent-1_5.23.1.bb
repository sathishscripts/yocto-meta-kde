# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/polkit-kde-agent-1-5.23.1.tar.xz"
SRC_URI[sha256sum] = "416617237e045567daf830e23c0d3f32f3b4d9891e9a61888f735f494ccbf529"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/systemsettings-5.23.1.tar.xz"
SRC_URI[sha256sum] = "7cd451319125c7b56c6f13c65462136179c39f607cc4d4e66b90079a72df6e54"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-nano-5.23.1.tar.xz"
SRC_URI[sha256sum] = "0a980893e7b928780ad9ff80b45e1c8706ce779e155fb43f664b0f3ea5086df9"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-integration-5.23.1.tar.xz"
SRC_URI[sha256sum] = "ca2b3487de16646bc5d2120adfd9d58cee69570a0eb64630ea075a93cdddcb71"


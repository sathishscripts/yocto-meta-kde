# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-nm-5.23.1.tar.xz"
SRC_URI[sha256sum] = "d1d578c0386c2506f2cf361cd81f7060e6edcc7546d1716fb7299e34332f5063"


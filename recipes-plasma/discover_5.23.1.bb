# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/discover-5.23.1.tar.xz"
SRC_URI[sha256sum] = "44255c9b1a8df70a4c710396ccbdaf1725f93694a040d4197e4bf45285e6ee71"


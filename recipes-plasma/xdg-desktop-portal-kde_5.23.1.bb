# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/xdg-desktop-portal-kde-5.23.1.tar.xz"
SRC_URI[sha256sum] = "f7664cc4c0ea7fcd93a15d35fc5f5b5f5c8910c07204fad35783f9d3acc1f2ae"


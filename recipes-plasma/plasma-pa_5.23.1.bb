# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-pa-5.23.1.tar.xz"
SRC_URI[sha256sum] = "a79f1971a5acdeccb9e7363560b1828f35cac4faeb3c8036d8cdd1243565a57e"


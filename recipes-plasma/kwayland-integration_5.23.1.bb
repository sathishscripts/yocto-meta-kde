# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kwayland-integration-5.23.1.tar.xz"
SRC_URI[sha256sum] = "f73510f65bff5dac0b9eb1715af58f7a95f312a9dbe816d853e78a9539c10d6a"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/breeze-5.23.1.tar.xz"
SRC_URI[sha256sum] = "ac11dc5f9347d870cbe33f2452bbc738e92e9d3dfc2dc8c2f5c0896476181481"


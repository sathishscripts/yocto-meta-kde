# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kwayland-server-5.23.1.tar.xz"
SRC_URI[sha256sum] = "ebad1a6dc1bf246cab9ca8ba0a13edd578646cb333ce09303d79d78727a59d57"


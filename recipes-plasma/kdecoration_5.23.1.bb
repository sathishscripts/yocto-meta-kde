# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kdecoration-5.23.1.tar.xz"
SRC_URI[sha256sum] = "1587aeab3743cb426318d4639e53b81c6227d114fc96b082b897cde6a2b0c327"


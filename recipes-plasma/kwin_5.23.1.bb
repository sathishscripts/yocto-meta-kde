# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kwin-5.23.1.tar.xz"
SRC_URI[sha256sum] = "5838d7ab84babc5bd3cb3df9f7e38fc01c98ba1bf584b6d43e4528dd03598b31"


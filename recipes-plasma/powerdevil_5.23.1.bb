# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/powerdevil-5.23.1.tar.xz"
SRC_URI[sha256sum] = "721a5938d6cb0439943fa912b9f360b9b563a91ceff138869636f2ef45edc096"


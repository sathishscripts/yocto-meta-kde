# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/milou-5.23.1.tar.xz"
SRC_URI[sha256sum] = "8f91c04785af3ed86ddf1fce18fe67f5b70cabf0827750cdec119e496723ff10"


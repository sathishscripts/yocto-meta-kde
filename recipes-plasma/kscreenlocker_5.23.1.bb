# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kscreenlocker-5.23.1.tar.xz"
SRC_URI[sha256sum] = "a0c41353285b41372eecce77d643e3332a34a391c52fb44064498f4321abb563"


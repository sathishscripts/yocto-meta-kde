# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kactivitymanagerd-5.23.1.tar.xz"
SRC_URI[sha256sum] = "eabc518b1476f17257c6f42c986611604903aa9efc42d35fd9d3fd15839fea09"


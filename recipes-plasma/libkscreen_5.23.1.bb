# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/libkscreen-5.23.1.tar.xz"
SRC_URI[sha256sum] = "3930d578f55e5d5be95da1f3fe298b15623afcea0a9c73005cb983589204d043"


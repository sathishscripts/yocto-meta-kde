# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/libksysguard-5.23.1.tar.xz"
SRC_URI[sha256sum] = "083ddf436a9678e6e3d9c900d2c974586a313e58f60a32b7cb6600338ec78189"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/kde-cli-tools-5.23.1.tar.xz"
SRC_URI[sha256sum] = "da1a08c54b3e085916c5157e1339ccb397e41dbfd68ce3334591e0ddbed0a7e2"


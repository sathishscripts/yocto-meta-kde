# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-systemmonitor-5.23.1.tar.xz"
SRC_URI[sha256sum] = "c8212484044840119f939ff2fcacea6a3d02c25a171a75c68145cf88be9c19cf"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/plasma-phone-components-5.23.1.tar.xz"
SRC_URI[sha256sum] = "82d6798fb17cb67013fbc10aa79cc9b6c49687bb1a139c80ad4eff2ccbda9c9c"


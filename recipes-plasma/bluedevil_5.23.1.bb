# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/plasma/5.23.1/bluedevil-5.23.1.tar.xz"
SRC_URI[sha256sum] = "2a3f42ee03d342673ac5ab4f91fd858f5d0003b798257d6cb7304c79c40eb4bb"


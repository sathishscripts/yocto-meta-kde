# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/gwenview-21.08.0.tar.xz"
SRC_URI[sha256sum] = "8d631791f532b7b674d0c701dd78395ff923c17683948ece5d7e17fe03c06f19"

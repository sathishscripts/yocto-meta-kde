# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/blinken-21.08.0.tar.xz"
SRC_URI[sha256sum] = "45288d7d4d7a03af621edd766fdb3bc8c86673cbaba2f42088778eb916d19844"

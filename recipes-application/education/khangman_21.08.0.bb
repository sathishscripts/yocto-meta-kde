# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/khangman-21.08.0.tar.xz"
SRC_URI[sha256sum] = "1072c76e38260f518aa27e4395943fad1219d252af7c142f487888b6c8fb6a0d"

# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/libkeduvocdocument-21.08.0.tar.xz"
SRC_URI[sha256sum] = "8d47469d3392f9451772592e131b472cd16eac10f3e8e6fcb0f268a59a3ba0da"

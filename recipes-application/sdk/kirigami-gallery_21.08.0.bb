# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kirigami-gallery-21.08.0.tar.xz"
SRC_URI[sha256sum] = "487753e629565953799050f4bfaa8bbbc53f7c352127c82ebf02d595155dc4e0"

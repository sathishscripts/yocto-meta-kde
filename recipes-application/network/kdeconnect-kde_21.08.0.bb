# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/release-service/21.08.0/src/kdeconnect-kde-21.08.0.tar.xz"
SRC_URI[sha256sum] = "9a0a31dd1af88735edbb06cfe7e11f4a42b7b31f12afa79c6e7479130060b1e2"
